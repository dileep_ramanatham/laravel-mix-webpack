
# Laravel Mix with Sass

A starter package with webpack repository.  For more detailed documentation, visit

https://laravel-mix.com/docs/6.0/examples


## Deployment

Install the packages

```bash
  npm install
```

Run and watch recompile on changes

```bash
  npm run watch
```

Run once without minification

```bash
  npm run dev / npm run development
```

Build Poduction / Distribuiton Build. The filename remains the same even with minification

```bash
  npm run prod / npm run production
```

### Autoprefixer

By default, Autoprefixer is enabled. You can learn more at 

https://laravel-mix.com/docs/6.0/autoprefixer
