let mix = require('laravel-mix');

//JSs files loading
mix.js('src/js/custom/*.js', 'dist/js/custom.js')
mix.js('src/js/vendor/*.js', 'dist/js/vendor.js')

//css files loading
mix.css('src/css/app.css', 'dist/css/app.css');

//Sass files loading
mix.sass('src/scss/style.scss', 'dist/css/style.css');

mix.browserSync({
    watch: true,
    files: [
        'dist/js/*',
        'dist/css/*',
        '*.html'
    ],
    proxy: {
        target: "http://laravel-mix-webpack.test/", // your domain
    },
    port: 3000, // your port
});
